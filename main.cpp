#include <pcap.h>
#include <stdio.h>
#include <stdint.h>
#include <arpa/inet.h>

void usage() {
  printf("syntax: pcap_test <interface>\n");
  printf("sample: pcap_test wlan0\n");
}

int main(int argc, char* argv[]) {
  if (argc != 2) {
    usage();
    return -1;
  }

  char* dev = argv[1];
  char errbuf[PCAP_ERRBUF_SIZE];
  pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
  if (handle == NULL) {
    fprintf(stderr, "couldn't open device %s: %s\n", dev, errbuf);
    return -1;
  }

  while (true) {
    struct pcap_pkthdr* header;
    const u_char* packet;
    int res = pcap_next_ex(handle, &header, &packet);
    if (res == 0) continue;
    if (res == -1 || res == -2) break;
    int i;
    const u_int16_t* l3_type = (u_int16_t*)(packet + 12);
    const u_int8_t* l4_type = (u_int8_t*)(packet + 23);
    const u_int16_t* src_port = (u_int16_t*)(packet + 14 + 20);
    const u_int16_t* des_port = (u_int16_t*)(packet + 14 + 22);
    u_int8_t tcp_length;
    printf("source mac: %02x %2x %02x %02x %02x %02x\n", packet[6], packet[7], packet[8], packet[9], packet[10], packet[11]);
    printf("destination mac: %02x %02x %02x %02x %02x %02x\n", packet[0], packet[1], packet[2], packet[3], packet[4], packet[5]);
    if (ntohs(*l3_type) == 0x800) {
      printf("L3 protocol is IP\n");
      printf("source ip: %u.%u.%u.%u\n", packet[14 + 12], packet[14 + 13], packet[14 + 14], packet[14 + 15]);
      printf("destination ip: %u.%u.%u.%u\n", packet[14 + 16], packet[14 + 17], packet[14 + 18], packet[14 + 19]);
      if(*l4_type == (u_int8_t)(6)) {
        printf("L4 protocol is TCP\n");
        printf("source port: %d\n", ntohs(*src_port));
        printf("destination port: %d\n", ntohs(*des_port));
        tcp_length =  ((packet[34 + 12] & (u_int8_t)(0xf0))) / 4; // multiply 4 to left 4byte(tcp len);
        if (header->caplen > 54) {
          printf("Data: ");
          for (i = 0; i < 30; i ++) {
            printf("%02x ", packet[34 + tcp_length + i]);
          }
          printf("\n");
        }
      } else if (*l4_type == (u_int8_t)(17)) {
        printf("L4 protocol is UDP\n");
      }
    }
    printf("=======================================\n");
  }

  pcap_close(handle);
  return 0;
}
